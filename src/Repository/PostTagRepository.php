<?php

namespace App\Repository;

use App\Entity\Post;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PostTagRepository
 * @package App\Repository
 */
class PostTagRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * PostTagRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $manager
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager, PostRepository $postRepository, TagRepository $tagRepository)
    {
        parent::__construct($registry, Post::class);
        $this->manager = $manager;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param array $data
     * @return string[]
     * @throws \Exception
     */
    public function saveNewTagInPost(array $data)
    {
        if (empty($data['post_id']) || empty($data['tag_id'])) {
            throw new \Exception("Expecting mandatory parameters!");
        }

        $post = $this->postRepository->getPostById($data['post_id']);
        $this->hasTagInPost($post->getTags(), $data['tag_id']);

        $tag = $this->tagRepository->getTagById($data['tag_id']);
        $post->addTag($tag);

        $this->manager->persist($post);
        $this->manager->flush();

        return [
            'message' => 'Tag included in post!'
        ];
    }

    /**
     * @param $tags
     * @param int $tagId
     * @return bool
     * @throws \Exception
     */
    private function hasTagInPost($tags, int $tagId): bool
    {
        foreach ($tags as $tagPost) {
            if ($tagPost->getId() === $tagId) {
                throw new \Exception("Already exist tag in post!");
            }
        }
        return true;
    }

    /**
     * @param $postId
     * @param $tagId
     * @return string[]
     * @throws \Exception
     */
    public function removePostTag(int $postId, int $tagId)
    {
        if (empty($postId) || empty($tagId)) {
            throw new \Exception("Expecting mandatory parameters!");
        }

        $post = $this->postRepository->getPostById($postId);
        $tag = $this->tagRepository->getTagById($tagId);
        $post->removeTag($tag);

        $this->manager->persist($post);
        $this->manager->flush();

        return [
            'message' => 'Tag removed in post!'
        ];
    }
}
