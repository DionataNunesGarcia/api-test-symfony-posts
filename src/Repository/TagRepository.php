<?php

namespace App\Repository;

use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * TagRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $manager
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Tag::class);
        $this->manager = $manager;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function saveNewTag(array $data): array
    {
        if (empty($data['name'])) {
            throw new \Exception("Expecting mandatory parameters!.");
        }

        $tag = new Tag();

        $tag->setName($data['name']);
        $tag->setSlug();
        $tag->setCreatedAt(new \DateTime('now'));
        $tag->setModifiedAt(new \DateTime('now'));

        $this->manager->persist($tag);
        $this->manager->flush();

        return [
            'message' => 'Tag created!',
            'data' => $tag->toArray()
        ];
    }

    /**
     * @param array $data
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function updatedTag(array $data, int $id): array
    {
        $tag = $this->findOneBy(['id' => $id]);
        if (empty($tag)) {
            throw new \Exception("Tag not found!.");
        }

        empty($data['name']) ? true : $tag->setName($data['name']);

        $tag->setSlug();
        $tag->setModifiedAt(new \DateTime('now'));

        $this->manager->persist($tag);
        $this->manager->flush();

        return [
            'message' => 'Tag updated!',
        ];
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeTag(int $id): void
    {
        $tag = $this->findOneBy(['id' => $id]);

        if (empty($tag)) {
            throw new \Exception("Tag not found!.");
        }

        $this->manager->remove($tag);
        $this->manager->flush();
    }

    /**
     * @return array|array[]
     */
    public function getTags()
    {
        $query = $this->findBy([], ['name' => 'ASC'], 5000, 1);

        return array_map(function ($item) {
            return $this->hydrateTag($item);
        }, $query);
    }

    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function getTag(int $id)
    {
        $post = $this->find($id);
        if (empty($post)) {
            throw new \Exception("Tag not found!.");
        }

        return $this->hydrateTag($post);
    }

    /**
     * @param Tag $tag
     * @return array
     */
    private function hydrateTag(Tag $tag): array
    {
        return [
            'name' => $tag->getName(),
            'slug' => $tag->getSlug(),
            'posts' => array_unique(array_map(function ($post) {
                return $post->getTitle();
            }, $tag->getPosts()->toArray())),
            'created_at' => $tag->getCreatedAt(),
            'modified_at' => $tag->getModifiedAt()
        ];
    }

    /**
     * @param int $tagId
     * @return Tag
     * @throws \Exception
     */
    public function getTagById(int $tagId): Tag
    {
        $tag = $this->findOneBy(['id' => $tagId]);
        if (empty($tag)) {
            throw new \Exception("Tag not found!");
        }
        return $tag;
    }
}
