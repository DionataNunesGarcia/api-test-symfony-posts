<?php

namespace App\Repository;

use App\Entity\Post;
use App\Entity\Tag;
use App\Repository\TagRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * PostRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $manager
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Post::class);
        $this->manager = $manager;
    }

    /**
     * @param int $id
     * @return Post
     * @throws \Exception
     */
    public function getPostById(int $id): Post
    {
        $post = $this->findOneBy(['id' => $id]);
        if (empty($post)) {
            throw new \Exception("Post not found");
        }
        return $post;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function saveNewPost(array $data): array
    {
        if (empty($data['title']) || empty($data['img_url'])) {
            throw new \Exception("Expecting mandatory parameters!.");
        }

        $post = new Post();

        $post->setTitle($data['title']);
        $post->setSlug();
        $post->setDescription($data['description']);
        $post->setContent($data['content']);
        $post->setImgUrl($data['img_url']);
        $post->setCreatedAt(new \DateTime('now'));
        $post->setModifiedAt(new \DateTime('now'));

        $this->manager->persist($post);
        $this->manager->flush();

        return [
            'message' => 'Post created!',
            'data' => $post->toArray()
        ];
    }

    /**
     * @param array $data
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function updatedPost(array $data, int $id): array
    {
        $post = $this->getPostById($id);

        empty($data['title']) ? true : $post->setTitle($data['title']);
        empty($data['description']) ? true : $post->setDescription($data['description']);
        empty($data['content']) ? true : $post->setContent($data['content']);
        empty($data['img_url']) ? true : $post->setImgUrl($data['img_url']);
        $post->setSlug();
        $post->setModifiedAt(new \DateTime('now'));

        $this->manager->persist($post);
        $this->manager->flush();

        return [
            'message' => 'Post updated!',
        ];
    }

    /**
     * @param int $id
     * @return string[]
     * @throws \Exception
     */
    public function removePost(int $id)
    {
        $post = $this->findOneBy(['id' => $id]);

        if (empty($post)) {
            throw new \Exception("Post not found!.");
        }

        $this->manager->remove($post);
        $this->manager->flush();

        return [
            'message' => 'Tag deleted!'
        ];
    }

    /**
     * @return array|array[]
     */
    public function getPosts()
    {
        $query = $this->findBy([], ['title' => 'ASC'], 5000, 1);

        return array_map(function ($item) {
            return $this->hydratePost($item);
        }, $query);
    }

    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function getPost(int $id)
    {
        $post = $this->find($id);
        if (empty($post)) {
            throw new \Exception("Post not found!.");
        }

        return $this->hydratePost($post);
    }

    /**
     * @param Post $post
     * @return array
     */
    private function hydratePost(Post $post): array
    {
        return [
            'title' => $post->getTitle(),
            'slug' => $post->getSlug(),
            'description' => $post->getDescription(),
            'content' => $post->getContent(),
            'tags' => array_unique(array_map(function ($tag) {
                return $tag->getName();
            }, $post->getTags()->toArray())),
            'created_at' => $post->getCreatedAt(),
            'modified_at' => $post->getModifiedAt()
        ];
    }
}
