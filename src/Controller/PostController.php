<?php

namespace App\Controller;

use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package App\Controller
 * @Route("/posts", name="post_")
 */
class PostController extends AbstractController
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     *
     * @Route("/", name="get_all", methods={"GET"})
     */
    public function getAll(Request $request, SerializerInterface $serializer): Response
    {
        $posts = $this->postRepository->getPosts();

        $response = [
            'status' => true,
            'data'   => $posts,
        ];

        return JsonResponse::fromJsonString($serializer->serialize($response, 'json'), Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @param SerializerInterface $serializer
     * @return Response
     *
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(int $id, SerializerInterface $serializer): Response
    {
        try {
            $post = $this->postRepository->getPost($id);
            $status = Response::HTTP_OK;

            $response = [
                'status' => true,
                'data' => $post,
            ];
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' => $ex->getMessage()
            ];
        }

        return JsonResponse::fromJsonString($serializer->serialize($response, 'json'), $status);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/", name="create", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $data  = $request->request->all();
            $response = $this->postRepository->saveNewPost($data);
            $status = Response::HTTP_CREATED;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' =>   $ex->getMessage()
            ];
        }

        return new JsonResponse($response, $status);
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/{id}", name="update", methods={"PUT", "PATCH"})
     */
    public function update($id, Request $request): JsonResponse
    {
        try {
            // TODO Receive nothing without data
            $data = $request->request->all();
            $response = $this->postRepository->updatedPost($data, $id);
            $status = Response::HTTP_OK;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' =>   $ex->getMessage()
            ];
        }

        return new JsonResponse($response, $status);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        try {
            $this->postRepository->removePost($id);
            $message = 'Post deleted!';
            $status = Response::HTTP_NO_CONTENT;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $message =   $ex->getMessage();
        }

        return new JsonResponse(['message' => $message], $status);
    }
}
