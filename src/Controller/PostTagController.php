<?php

namespace App\Controller;

use App\Repository\PostTagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class PostTagController
 * @package App\Controller
 * @Route("/posts-tags", name="post_tag_")
 */
class PostTagController extends AbstractController
{
    /**
     * @var PostTagRepository
     */
    private $postTagRepository;

    /**
     * PostTagController constructor.
     * @param PostTagRepository $postTagRepository
     */
    public function __construct(PostTagRepository $postTagRepository)
    {
        $this->postTagRepository = $postTagRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/", name="create", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $data  = $request->request->all();
            $response = $this->postTagRepository->saveNewTagInPost($data);
            $status = Response::HTTP_CREATED;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' =>   $ex->getMessage()
            ];
        }

        return new JsonResponse($response, $status);
    }

    /**
     * @param $postId
     * @param $tagId
     * @return JsonResponse
     *
     * @Route("/{postId}/{tagId}", name="delete", methods={"DELETE"})
     */
    public function delete($postId, $tagId): JsonResponse
    {
        try {
            $this->postTagRepository->removePostTag($postId, $tagId);
            $message = 'PostTag deleted!';
            $status = Response::HTTP_NO_CONTENT;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $message =   $ex->getMessage();
        }

        return new JsonResponse(['message' => $message], $status);
    }
}
