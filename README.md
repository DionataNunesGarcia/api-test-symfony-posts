# Test Symfony - Possible

Test Backend PHP in Symfony 

## Starting

### Installing

Clone Project

```
git clone git@gitlab.com:DionataNunesGarcia/test-symfony-possible.git
```


## Technologies

- [Symfony 5.2](https://symfony.com/) - The PHP framework used
- [Php 7.4](https://secure.php.net/docs.php/) - Programming Language
- [Mysql 5.7](https://www.mysql.com/) - Database


### Install Symfony

```
https://symfony.com/download
```
### Bundles Used

- maker --dev
- orm


### Executed

- Create Database (db: api_symfony)

```
php bin/console doctrine:database:create
```

- Migrate Tables
```
php bin/console doctrine:migrations:migrate
```

## AUTH JWT

https://github.com/lexik/LexikJWTAuthenticationBundle
```
$ mkdir -p config/jwt
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```
### Register User
```
POST: http://127.0.0.1:8000/register
{
    "email": "admin@admin.com",
    "password": "your_pass",
    "roles": "['ROLE_USER']"
}
```
### Check Login
- POST
```
http://127.0.0.1:8000/api/login_check
{
    "username": "admin@admin.com",
    "password": "your_pass"
}
```
- Response
```
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2MDczNTgyMDksImV4cCI6MTYwNzM2MTgwOSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiYWRtaW5AYWRtaW4uY29tIn0.nz5bpltJyHF1hcyceaayDqEppjuu6L14RMGJPQiE04sN6nJghCtnpvqV77GRonRHasvDeU6UN8scylJa-8EVjU0f0tx0Bn4tEBOjziAyj88i1X-OsjvKjP9webANIiS420x9VJVFkBQ-LFHFCwSW2oIri__fHg7g_exz_YX0D-Xkm5asBZJTOk83r2Ewp9HJhnpL0Av4FbpruIudrkX7UBkJWQalEz3XEAQPVxQakwMJeSmiRMnUGVDUjlN31ytauFMCfmdfHWLer7S8sjEEQOqRB-0WfisIwQ9JyYndXFayuS6FVsC1OYUeT-qodyp1V01hXADe6H5YeAbJ1wUSkoZsAEJwGeINilnpiATQkNampVDVy4BdKvQILS8JZhTQ6f6hTHGJTiqZQS-rZQdSFhtTWGlfIodteLsprt3Ykm08NX8xwe6yQt7nqsztS6dNsfxXV1yGn3e-5VtsWoMoQ2S5v-80UYicN6WmMwbQowFJZ2K3yG-96j5f-DVBLohhJl62BrejMhcU_kCOZkVczrH1kQr05oy6h16M7c9CWD673PDmZxphLg4UHiyCS7sXyW8PqzULrvN5mVm5IsfzAZGWW39lrSUQPpO3m6whrDONXkS3qNAkt4tGUz6LuaLX2cVjxDjf-IZOApxwEVE5-DgpsP7SHICwGg--443QvmM"
}
```

Authorization: Bearer token


## Tasks

### Task 1: 
#### CRUD for the Post entity. {Create, Read, Update, and Delete}
SHOW ALL - GET - http://127.0.0.1:8000/posts/
```
{
    "status": true,
    "data": [
        {
            "id": 1,
            "title": "First Post",
            "slug": "first-post",
            "description": "description first post",
            "content": "",
            "imgUrl": "https://picsum.photos/200/300",
            "createdAt": "2020-12-04T02:48:30+00:00",
            "modifiedAt": "2020-12-05T01:41:51+00:00"
        },
        {
            "id": 2,
            "title": "Second Post",
            "slug": "second-post",
            "description": "description second post",
            "content": "",
            "imgUrl": "https://picsum.photos/200",
            "createdAt": "2020-12-04T02:51:02+00:00",
            "modifiedAt": "2020-12-04T02:51:02+00:00"
        },
    ]
}
```
---
READING - GET - http://127.0.0.1:8000/posts/1
```
{
    "status": true,
    "data": {
        "title": "Post number #110",
        "description": null,
        "content": null,
        "tags": [
            "Ipsum",
            "Sit",
            "Elit",
            "Tempor",
            "Dolore"
        ],
        "created_at": "2020-12-06T21:19:38+00:00",
        "modified_at": "2020-12-06T21:19:38+00:00"
    }
}
```
---
CREATED - POST - http://127.0.0.1:8000/posts/
```
data: {
    "title": "Title Post",
    "description": "Desciption Post",
    "content": "Content Post",
}
```
---
UPDATED - PUT | PATCH - http://127.0.0.1:8000/posts/1
```
data: {
    "title": "Title Post modified",
    "description": "Desciption Post modified",
    "content": "Content Post modified",
}
```
---
DELETED - DELETE - http://127.0.0.1:8000/posts/1

### Task 2:
#### CRUD for the Tag entity. {Create, Read, Update, and Delete}
SHOW ALL - GET - http://127.0.0.1:8000/tags/
```
{
    "status": true,
    "data": [
        {
            "name": "Amend",
            "slug": "amend",
            "posts": [
                "Post number #4716"
            ],
            "created_at": "2020-12-06T21:19:44+00:00",
            "modified_at": "2020-12-06T21:19:44+00:00"
        },
        {
            "name": "Amend",
            "slug": "amend",
            "posts": [
                "Post number #4797"
            ],
            "created_at": "2020-12-06T21:19:44+00:00",
            "modified_at": "2020-12-06T21:19:44+00:00"
        },
    ]
}
```
---
READING - GET - http://127.0.0.1:8000/tags/1
```
{
    "status": true,
    "data": {
        "name": "Ipsum",
        "slug": "ipsum-121",
        "posts": [
            "Post number #47"
        ],
        "created_at": "2020-12-06T21:19:38+00:00",
        "modified_at": "2020-12-07T01:27:44+00:00"
    }
}
```
---
CREATED - POST - http://127.0.0.1:8000/tags/
```
data: {
    "name": "Title Post",
}
```
---
UPDATED - PUT | PATCH - http://127.0.0.1:8000/tags/1
```
data: {
    "name": "Title Post modified",
}
```
---
DELETED - DELETE - http://127.0.0.1:8000/tags/1

---
### Task 3: 
#### CRUD for the Post Tag relation entity. {Create, Read, Update, and Delete};
CREATED - POST - http://127.0.0.1:8000/tags/
```
data: {
    "post_id": {postId},
    "tag_id": {tagId},
}
```

DELETED - DELETE - http://127.0.0.1:8000/posts-tags/{postId}/{tagId}

Note: Update and Read did not understand how it would have to be done.

---
### Task 4: 
#### Posts and Tags must be translatable in the supported languages
It was not developed

---
### Task 5: 
#### Create a custom module/plugin to import a CSV file with several rows (5k+) to save posts and relation with tags. CSV is also on the email with this PDF
```
php bin/console ImportPosts public/files/import/posts-and-tags.csv
```


Postman Collection
public/files/PostmanTests/TestSyfomyPossible.postman_collection