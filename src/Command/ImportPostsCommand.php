<?php

namespace App\Command;

use App\Entity\Post;
use App\Entity\Tag;
use App\Repository\TagRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;


class ImportPostsCommand extends Command
{
    protected static $defaultName = 'ImportPosts';

    private $manager;

    private $tagRepository;

    private $io;

    private $filename;

    public function __construct(EntityManagerInterface $manager, TagRepository $tagRepository)
    {
        parent::__construct();

        $this->manager = $manager;
        $this->tagRepository = $tagRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Update posts records')
            ->addArgument('file', InputArgument::REQUIRED, 'Location of the CSV-file to read products from.');
    }

    /**
     * @param $input
     * @param $output
     */
    public function setIo($input, $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename): void
    {
        $this->filename = $filename;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filename = $input->getArgument('file');
        $this->setFilename($filename);

        $this->setIo($input, $output);

        $this->io->text([
            'Reads a CSV file and imports the contained posts into our database.',
            'This command will alter your database! Please be careful when using it in production.',
        ]);
        $this->io->newLine();

        if (!$this->validateFile()) {
            return Command::FAILURE;
        }

        $this->saveAllPosts();

        $this->manager->flush();

        return Command::SUCCESS;
    }

    /**
     *
     */
    private function saveAllPosts()
    {
        //Convert file to array
        $csvArray = $this->convertFileToArray($this->filename, ',', true);

        //init count
        $count = 0;
        foreach ($csvArray as $data) {

            $count++;
            //every 100, write the counter
            if ($count == 1 || ($count % 100) == 0) {
                $this->io->write("##imported the total {$count} Posts \n");
            }

            if (count($data) <= 1) {
                break;
            }

            $this->savePost($data);
        }
    }

    /**
     * @return bool
     */
    private function validateFile(): bool
    {
        if(!file_exists($this->filename) || !is_readable($this->filename)) {
            $this->io->error(sprintf('The provided filename "%s" is not readable!', $this->filename));
            return false;
        }
        return true;
    }

    /**
     * @param $csvFile
     * @param string $separated
     * @param false $removeFirstLine
     * @return array
     */
    private function convertFileToArray($csvFile, $separated = ';', $removeFirstLine = false)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time','600');
        ini_set('max_allowed_packet','512MB');
        $csv = explode("\n", file_get_contents($csvFile));
        $csvArray = [];
        foreach ($csv as $key => $line) {
            $linhe = str_getcsv($line, $separated);
            $csvArray[$key] = $linhe;
        }

        //Remove first line (header)
        if ($removeFirstLine) {
            array_shift($csvArray);
        }

        return $csvArray;
    }

    private function savePost(array $data)
    {
        $post = new Post();
        $post->setTitle($data[0]);
        $post->setSlug();
        $post->getDescription(!empty($data[1]) ? $data[1] : '');
        $post->setImgUrl($data[2]);
        $post->setCreatedAt(new \DateTime('now'));
        $post->setModifiedAt(new \DateTime('now'));

        $tagsNames = explode(';', $data[3]);
        foreach ($tagsNames as $tagName) {
            $tag = $this->convertNameToTag(trim($tagName));
            if ($tag) {
                $post->addTag($tag);
            }
        }

        $this->manager->persist($post);
    }

    /**
     * @param string $name
     * @return Tag|null
     */
    public function convertNameToTag(string $name): ?Tag
    {
        if (!$name) {
            return null;
        }

        $tag = $this->tagRepository->findOneBy(['name' => $name]);

        if (!$tag) {
            $tag = new Tag();
            $tag->setName($name);
            $tag->setSlug();
            $tag->setCreatedAt(new \DateTime('now'));
            $tag->setModifiedAt(new \DateTime('now'));

            $this->manager->persist($tag);

            $this->manager->flush();
        }

        return $tag;
    }
}
