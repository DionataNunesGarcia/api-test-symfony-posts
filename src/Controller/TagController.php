<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Form\TagType;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/tags", name="tag_")
 */
class TagController extends AbstractController
{
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * TagController constructor.
     * @param TagRepository $tagRepository
     */
    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route("/", name="get_all", methods={"GET"})
     */
    public function getAll(SerializerInterface $serializer): Response
    {
        $tags = $this->tagRepository->getTags();

        $response = [
            'status' => true,
            'data'   => $tags,
        ];

        return JsonResponse::fromJsonString($serializer->serialize($response, 'json'), Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @param SerializerInterface $serializer
     * @return Response
     * @throws \Exception
     *
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(int $id, SerializerInterface $serializer): Response
    {
        try {
            $tag = $this->tagRepository->getTag($id);
            $response = [
                'status' => true,
                'data'   => $tag,
            ];
            $status = Response::HTTP_CREATED;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' =>   $ex->getMessage()
            ];
        }

        return JsonResponse::fromJsonString($serializer->serialize($response, 'json'), $status);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/", name="create", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $data  = $request->request->all();
            $response = $this->tagRepository->saveNewTag($data);
            $status = Response::HTTP_CREATED;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' =>   $ex->getMessage()
            ];
        }

        return new JsonResponse($response, $status);
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/{id}", name="update", methods={"PUT", "PATCH"})
     */
    public function update($id, Request $request): JsonResponse
    {
        try {
            // TODO Receive nothing without data
            $data  = $request->request->all();
            $response = $this->tagRepository->updatedTag($data, $id);
            $status = Response::HTTP_OK;

        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' =>   $ex->getMessage()
            ];
        }

        return new JsonResponse($response, $status);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        try {
            $response = $this->tagRepository->removeTag($id);
            $status = Response::HTTP_NO_CONTENT;
        } catch (\Exception $ex) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'message' => $ex->getMessage()
            ];
        }

        return new JsonResponse($response, $status);
    }
}
